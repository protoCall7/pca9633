/*
 * pca9633.h
 * RGB backlight driver for Waveshare 1602 LCD module
 * Peter H. Ezetta
 * 1/2023
 */

#ifndef __PCA9633__
#define __PCA9633__

#include "stm32f3xx.h"

#define PCA9633_I2C_ADDR (0x60 << 1) /* LCD1602 RGB Datasheet Pg. 6 */

/* PCA9633 Register Map (PCA9633 Datasheet Pg. 11) */
#define PCA9633_MODE1		0x0
#define PCA9633_MODE2		0x1
#define PCA9633_PWM0		0x2
#define PCA9633_PWM1		0x3
#define PCA9633_PWM2		0x4
#define PCA9633_PWM3		0x5
#define PCA9633_GRPPWM		0x6
#define PCA9633_GRPFREQ		0x7
#define PCA9633_LEDOUT		0x8
#define PCA9633_SUBADR1		0x9
#define PCA9633_SUBADR2		0xA
#define PCA9633_SUBADR3		0xB
#define PCA9633_ALLCALLADR	0xC

#define PCA9633_MODE2_OUTNE	0x0
#define PCA9633_MODE2_OUTDRV	0x2
#define PCA9633_MODE2_OCH	0x3
#define PCA9633_MODE2_INVRT	0x4
#define PCA9633_MODE2_DMBLNK	0x5

/* device type */
typedef struct {
	I2C_HandleTypeDef *i2chandle;
} PCA9633;

/* function prototypes */
uint8_t PCA9633_init(PCA9633 *, I2C_HandleTypeDef *);
uint8_t PCA9633_linearize(uint8_t);
void PCA9633_setrgba(PCA9633 *, uint8_t, uint8_t, uint8_t, uint8_t);
HAL_StatusTypeDef PCA9633_read_register(PCA9633 *, uint8_t, uint8_t *);
HAL_StatusTypeDef PCA9633_read_registers(PCA9633 *, uint8_t, uint8_t *, uint8_t);
HAL_StatusTypeDef PCA9633_write_register(PCA9633 *, uint8_t, uint8_t *);

#endif
